package com.mrhc.logback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogbackDemo {

    private static Logger logger = LoggerFactory.getLogger(LogbackDemo.class);

    public static void main(String[] args) {
        logger.trace("========trace");
        logger.info("========info");
        logger.warn("========warn");
        logger.error("========error");

       /* String name = "Aub";
        String message = "3Q";
        String[] fruits = {"apple", "banana", "orange"};

        logger.info("Hello,{}!", name);
        logger.info("Hello,{}! {}!", name, message);
        StringBuilder sb = new StringBuilder();
        for (String fruit : fruits) {
            sb.append("{}" + ",");
        }
        logger.info(sb.toString(), fruits);*/
    }
}
