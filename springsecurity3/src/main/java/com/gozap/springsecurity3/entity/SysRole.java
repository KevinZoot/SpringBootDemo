package com.gozap.springsecurity3.entity;

import javax.persistence.*;

@Entity
public class SysRole {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    public SysRole(String name) {
        this.name = name;
    }

    public SysRole() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
