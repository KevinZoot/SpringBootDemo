package com.gozap.springsecurity3.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class SysResource {
    @Id
    @GeneratedValue
    private Long id;

    private String resourceString;

    @ManyToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
    private List<SysRole> roles;

    public SysResource() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResourceString() {
        return resourceString;
    }

    public void setResourceString(String resourceString) {
        this.resourceString = resourceString;
    }

    public List<SysRole> getRoles() {
        return roles;
    }

    public void setRoles(List<SysRole> roles) {
        this.roles = roles;
    }

    public SysResource(String resourceString, List<SysRole> roles) {
        this.resourceString = resourceString;
        this.roles = roles;
    }
}
