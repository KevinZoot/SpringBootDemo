package com.gozap.springsecurity3.service;

import com.gozap.springsecurity3.entity.SecurityUser;
import com.gozap.springsecurity3.entity.SysUser;
import com.gozap.springsecurity3.repository.SysUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomerUserDetailService implements UserDetailsService {

    @Autowired
    SysUserRepository sysUserRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        SysUser user = sysUserRepository.findByName(s);
        if (user == null) {
            throw new UsernameNotFoundException("userName" + s + "not found");
        }
        SecurityUser securityUser = new SecurityUser(user);
        return securityUser;
    }
}
