package com.gozap.springsecurity3.repository;

import com.gozap.springsecurity3.entity.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SysUserRepository extends JpaRepository<SysUser, Long> {

    SysUser findByName(String name);
}
