package com.mrhc.firstspringboot;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RestController
@SpringBootApplication
public class FirstspringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(FirstspringbootApplication.class, args);
    }

    @Value("${chen.name}")
    String name;
    @Value("${chen.age}")
    int age;
    @Value("${chen.date}")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    Date date;

    @RequestMapping("/")
    String index() {
        return name + age + date;
    }
}
