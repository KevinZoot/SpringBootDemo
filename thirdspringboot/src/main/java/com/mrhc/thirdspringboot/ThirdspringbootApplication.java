package com.mrhc.thirdspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class ThirdspringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThirdspringbootApplication.class, args);
    }

    @RequestMapping("/")
    public String index() {
        return "Hello";
    }
}
